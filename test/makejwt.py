#!/usr/bin/env python

from itsdangerous import (TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired)
import datetime, time
s = Serializer('0eb61f396824dacc3c1923e8c27ad1ad', expires_in=3600)
token = s.dumps({'sub': 'caxton'})
print(token.decode('utf-8'))
