if [ -z  "$TEST_HOST" ]
then

    echo
    echo "No TEST_HOST set. Run 'export TEST_HOST=<the_hostname>' to set it.";
    echo "This defaults to localhost";
    echo
    echo

fi

if [ -z "$TEST_PORT" ]
then
    echo "No TEST_PORT set. Run export TEST_PORT=<the_port>";
    echo "This defaults to 8000";
fi

# Guzzle
echo
echo "Fetching Guzzle Catalogues"
echo
echo
curl -H "Accept: application/xml" -H "Authorization: Bearer `./makejwt.py`" "http://${TEST_HOST:-localhost}:${TEST_PORT:-8000}/api/v1.0/guzzle/catalogue?format=xml"
echo
echo "Fetching Guzzle Categories"
echo
echo
curl -H "Accept: application/xml" -H "Authorization: Bearer `./makejwt.py`" "http://${TEST_HOST:-localhost}:${TEST_PORT:-8000}/api/v1.0/guzzle/category?format=xml"
echo
echo "Fetching Guzzle Locations"
echo
echo
curl -H "Accept: application/xml" -H "Authorization: Bearer `./makejwt.py`" "http://${TEST_HOST:-localhost}:${TEST_PORT:-8000}/api/v1.0/guzzle/location?format=xml"
echo
echo "Fetching Guzzle Retailers"
echo
echo
curl -H "Accept: application/xml" -H "Authorization: Bearer `./makejwt.py`" "http://${TEST_HOST:-localhost}:${TEST_PORT:-8000}/api/v1.0/guzzle/retailer?format=xml"

# Private Property
echo
echo "Fetching Private Property 3 Bedroom For Sale listings for Heidelberg Nigel Heraut"
echo
echo
curl -H "Accept: application/xml" -H "Authorization: Bearer `./makejwt.py`" "http://${TEST_HOST:-localhost}:${TEST_PORT:-8000}/api/v1.0/property/sale?customArea=heidelbergnigelheraut&beds=3&take=30&page=1"

# CARMag
echo
echo "Fetching CARMag VW used cars listings for Heidelberg Nigel Heraut"
echo
echo
curl -H "Accept: application/xml" -H "Authorization: Bearer `./makejwt.py`" "http://${TEST_HOST:-localhost}:${TEST_PORT:-8000}/api/v1.0/vehicle?location_names=Heidelberg,Nigel&make=Volkswagen&type=used_cars"
curl -H "Accept: application/xml" -H "Authorization: Bearer `./makejwt.py`" "http://${TEST_HOST:-localhost}:${TEST_PORT:-8000}/api/v1.0/vehicle?make=Volkswagen&model_name=Syncro&start_year=1990&end_year=2000&type=used_cars"
curl -H "Accept: application/xml" -H "Authorization: Bearer `./makejwt.py`" "http://${TEST_HOST:-localhost}:${TEST_PORT:-8000}/api/v1.0/vehicle?location_names=Roodepoort&type=used_cars"
curl -H "Accept: application/xml" -H "Authorization: Bearer `./makejwt.py`" "http://${TEST_HOST:-localhost}:${TEST_PORT:-8000}/api/v1.0/vehicle?location_names=Pietermaritzburg,Pinetown,Mooirivier&type=new_cars"
curl -H "Accept: application/xml" -H "Authorization: Bearer `./makejwt.py`" "http://${TEST_HOST:-localhost}:${TEST_PORT:-8000}/api/v1.0/vehicle?format=xml&page=1&cars_on_page=20&location_names=Brakpan&order_by=1&current_page=1&type=all_cars"

# CARMag News
curl -H "Accept: application/xml" -H "Authorization: Bearer `./makejwt.py`" "http://${TEST_HOST:-localhost}:${TEST_PORT:-8000}/api/v1.0/vehiclenews"
curl -H "Accept: application/xml" -H "Authorization: Bearer `./makejwt.py`" "http://${TEST_HOST:-localhost}:${TEST_PORT:-8000}/api/v1.0/vehiclenews?category=road-tests"
curl -H "Accept: application/xml" -H "Authorization: Bearer `./makejwt.py`" "http://${TEST_HOST:-localhost}:${TEST_PORT:-8000}/api/v1.0/vehiclenews?category=road-tests&articles_per_page=5&page=1"

# DAM
curl -H "Accept: application/xml" -H "Authorization: Bearer `./makejwt.py`" "http://${TEST_HOST:-localhost}:${TEST_PORT:-8000}/api/v1.0/dam?offset=0&size=100"
