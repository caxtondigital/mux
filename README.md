# Caxton MUX (Multiplexer)


### Property endpoint
- curl -H "Accept: application/xml" -H "Authorization: Bearer `./test/makejwt.py`" "http://localhost:8000/api/v1.0/property/sale?customArea=heidelbergnigelheraut&beds=3&take=30&page=1"


### Catalogue endpoints

The 4 Catalogue endpoints accept the exact same parameters as per the Guzzle API documentation. 

- curl -H "Accept: application/xml" -H "Authorization: Bearer `./test/makejwt.py`" "http://localhost:8000/api/v1.0/guzzle/catalogue?format=xml" 
- curl -H "Accept: application/xml" -H "Authorization: Bearer `./test/makejwt.py`" "http://localhost:8000/api/v1.0/guzzle/category?format=xml" 
- curl -H "Accept: application/xml" -H "Authorization: Bearer `./test/makejwt.py`" "http://localhost:8000/api/v1.0/guzzle/location?format=xml" 
- curl -H "Accept: application/xml" -H "Authorization: Bearer `./test/makejwt.py`" "http://localhost:8000/api/v1.0/guzzle/retailer?format=xml" 


