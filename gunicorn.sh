#!/bin/sh

NAME=mux
USER=root
GROUP=root
WORKERS=16
SOCKFILE=/var/run/mux.sock                              # we will communicte using this unix socket
echo "Starting mux as root"

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

#export LC_ALL=en_US.UTF-8
#export LANG=en_US.UTF-8

# Start your Flask Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec gunicorn -c /app/gconfig.py wsgi:app --reload --timeout 60 --name=$NAME --user=$USER --group=$GROUP --workers=$WORKERS -b 0.0.0.0:8000 --bind=unix:$SOCKFILE --log-level=debug

