import os
from flask import abort, make_response, g
from flask_restful import Resource, reqparse, fields, marshal
from ..shared.Authentication import token_auth, secret_auth, multi_auth
from ..models.Retailer import Retailer, RetailerSchema
from ..models.Location import Location, LocationSchema
from ..models.Catalogue import Catalogue, CatalogueSchema
from ..models.Category import Category, CategorySchema
from . import catalogue_api

class CatalogueAPI(Resource):
    """
        User API v1.0 for the Catalogue consumer
    """
    catalogue_schema = CatalogueSchema()

    def __init__(self):
        ''' Setup the API '''
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('format', type=str, required=True, default='xml', location='args')
        self.reqparse.add_argument('gps', type=str, location='args')
        self.reqparse.add_argument('offset', type=int, default=0, location='args')
        self.reqparse.add_argument('order_by', type=str, default='id', location='args')
        self.reqparse.add_argument('limit', type=int, default=10, location='args')
        self.reqparse.add_argument('paying_or_trial', type=bool, location='args')
        self.reqparse.add_argument('supplier_id', type=int, location='args')
        self.reqparse.add_argument('category_id', type=int, location='args')
        self.reqparse.add_argument('category_foreign_key_id', type=int, location='args')
        self.reqparse.add_argument('province', type=str, location='args')
        super(CatalogueAPI, self).__init__()

    @token_auth.login_required
    def get(self):
        ''' 
            Query Catalogue consumer. 
            ------------------------
            HTTP Request method: GET
            Uses token_auth only. Needs Authorization HTTP Header with JWT Bearer token and an empty payload.
            Returns a XML when Header 'Accept: text/xml' exists.
            +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        '''
        params = self.reqparse.parse_args()
        api = Catalogue()
        return api.query(params = params), 200


class CategoryAPI(Resource):
    """
        User API v1.0 for the Category consumer
    """
    category_schema = CategorySchema()

    def __init__(self):
        ''' Setup the API '''
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('format', type=str, required=True, default='xml', location='args')
        self.reqparse.add_argument('offset', type=int, default=0, location='args')
        self.reqparse.add_argument('limit', type=int, default=10, location='args')
        self.reqparse.add_argument('catalogue_id', type=int, location='args')
        self.reqparse.add_argument('parent_category_id', type=int, location='args')
        super(CategoryAPI, self).__init__()

    @token_auth.login_required
    def get(self):
        ''' 
            Query Category consumer. 
            ------------------------
            HTTP Request method: GET
            Uses token_auth only. Needs Authorization HTTP Header with JWT Bearer token and an empty payload.
            Returns a XML when Header 'Accept: text/xml' exists.
            +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        '''
        params = self.reqparse.parse_args()
        api = Category()
        return api.query(params = params), 200


class LocationAPI(Resource):
    """
        User API v1.0 for the Location consumer
    """
    location_schema = LocationSchema()

    def __init__(self):
        ''' Setup the API '''
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('format', type=str, required=True, default='xml', location='args')
        self.reqparse.add_argument('order_by', type=str, default='city', location='args')
        self.reqparse.add_argument('visible', type=str, default='true', location='args')
        self.reqparse.add_argument('offset', type=int, default=0, location='args')
        self.reqparse.add_argument('limit', type=int, default=10, location='args')
        super(LocationAPI, self).__init__()

    @token_auth.login_required
    def get(self):
        ''' 
            Query Location consumer. 
            ------------------------
            HTTP Request method: GET
            Uses token_auth only. Needs Authorization HTTP Header with JWT Bearer token and an empty payload.
            Returns a XML when Header 'Accept: text/xml' exists.
            +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        '''
        params = self.reqparse.parse_args()
        api = Location()
        return api.query(params = params), 200


class RetailerAPI(Resource):
    """
        User API v1.0 for the Retailer consumer
    """
    retailer_schema = RetailerSchema()

    def __init__(self):
        ''' Setup the API '''
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('format', type=str, required=True, default='xml', location='args')
        self.reqparse.add_argument('gps', type=str, location='args')
        self.reqparse.add_argument('radius', type=float, location='args')
        self.reqparse.add_argument('order_by', type=str, default='name', location='args')
        self.reqparse.add_argument('offset', type=int, default=0, location='args')
        self.reqparse.add_argument('limit', type=int, default=10, location='args')
        super(RetailerAPI, self).__init__()

    @token_auth.login_required
    def get(self):
        ''' 
            Query Retailer consumer. 
            ------------------------
            HTTP Request method: GET
            Uses token_auth only. Needs Authorization HTTP Header with JWT Bearer token and an empty payload.
            Returns a XML when Header 'Accept: text/xml' exists.
            +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        '''
        params = self.reqparse.parse_args()
        api = Retailer()
        return api.query(params = params), 200


catalogue_api.add_resource(CatalogueAPI, '/api/v1.0/guzzle/catalogue', endpoint='guzzle.catalogue')
catalogue_api.add_resource(CategoryAPI, '/api/v1.0/guzzle/category', endpoint='guzzle.category')
catalogue_api.add_resource(LocationAPI, '/api/v1.0/guzzle/location', endpoint='guzzle.location')
catalogue_api.add_resource(RetailerAPI, '/api/v1.0/guzzle/retailer', endpoint='guzzle.retailer')

''' Requests MUST include header "Accept: text/xml" otherwise the response will break '''
@catalogue_api.representation('application/xml')
def output_xml(data, code, headers=None):
    resp = make_response(data, code)
    resp.headers.extend(headers or {})
    return resp

