import os
from flask import abort, make_response, g
from flask_restful import Resource, reqparse, fields, marshal
from ..shared.Authentication import token_auth, secret_auth, multi_auth
from ..models.Property import Property, PropertySchema
from . import property_api

class PropertyAPI(Resource):
    """
        User API v1.0 for the Property consumer
    """
    property_schema = PropertySchema()

    def __init__(self):
        ''' Setup the API '''
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('customArea', type=str, required=True, location='args')
        self.reqparse.add_argument('sortBy', type=str, default='Default', location='args')
        self.reqparse.add_argument('page', type=int, default=1, location='args')
        self.reqparse.add_argument('take', type=int, default=30, location='args')
        self.reqparse.add_argument('priceFrom', type=int, location='args')
        self.reqparse.add_argument('priceTo', type=int, location='args')
        self.reqparse.add_argument('suburbIds', type=str, location='args')
        self.reqparse.add_argument('beds', type=int, location='args')
        self.reqparse.add_argument('baths', type=int, location='args')
        self.reqparse.add_argument('garages', type=int, location='args')
        self.reqparse.add_argument('baths', type=int, location='args')
        self.reqparse.add_argument('propertyTypeGroup', type=str, location='args')
        super(PropertyAPI, self).__init__()

    @token_auth.login_required
    def get(self, transaction_type = None):
        ''' 
            Query Property consumer. 
            ------------------------
            HTTP Request method: GET
            Uses token_auth only. Needs Authorization HTTP Header with JWT Bearer token and an empty payload.
            Returns a XML when Header 'Accept: text/xml' exists.
            +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        '''
        params = self.reqparse.parse_args()
        api = Property()
        return api.query(transactionType = transaction_type, params = params), 200

property_api.add_resource(PropertyAPI, '/api/v1.0/property/<string:transaction_type>', endpoint='property')

''' Requests MUST include header "Accept: text/xml" otherwise the response will break '''
@property_api.representation('application/xml')
def output_xml(data, code, headers=None):
    resp = make_response(data, code)
    resp.headers.extend(headers or {})
    return resp

