import os
from flask import abort, make_response, g
from flask_restful import Resource, reqparse, fields, marshal
from ..shared.Authentication import token_auth, secret_auth, multi_auth
from ..models.Vehicle import Vehicle, VehicleSchema
from ..models.VehicleNews import VehicleNews, VehicleNewsSchema
from . import vehicle_api

class VehicleAPI(Resource):
    """
        User API v1.0 for the Vehicle consumer
    """
    vehicle_schema = VehicleSchema()

    def __init__(self):
        ''' Setup the API '''
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('make', type=str, location='args')
        self.reqparse.add_argument('model_name', type=str, location='args')
        self.reqparse.add_argument('order_by', type=int, default=3, location='args')
        self.reqparse.add_argument('bodytype', type=str, location='args')
        self.reqparse.add_argument('page', type=int, default=1, location='args')
        self.reqparse.add_argument('cars_on_page', type=int, default=20, location='args')
        self.reqparse.add_argument('start_year', type=int, location='args')
        self.reqparse.add_argument('end_year', type=int, location='args')
        self.reqparse.add_argument('start_price', type=int, location='args')
        self.reqparse.add_argument('end_price', type=int, location='args')
        self.reqparse.add_argument('start_mileage', type=int, location='args')
        self.reqparse.add_argument('end_mileage', type=int, location='args')
        self.reqparse.add_argument('type', type=str, default='all_cars', location='args')
        self.reqparse.add_argument('transmission', type=str, location='args')
        self.reqparse.add_argument('fuel', type=str, location='args')
        self.reqparse.add_argument('location_names', type=str, location='args')
        self.reqparse.add_argument('locations', type=str, location='args')
        self.reqparse.add_argument('featured', type=str, default='Both', location='args')
        self.reqparse.add_argument('method', type=str, default='catchment', location='args')
        super(VehicleAPI, self).__init__()

    @token_auth.login_required
    def get(self):
        ''' 
            Query Vehicle consumer. 
            ------------------------
            HTTP Request method: GET
            Uses token_auth only. Needs Authorization HTTP Header with JWT Bearer token and an empty payload.
            Returns a XML when Header 'Accept: text/xml' exists.
            +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        '''
        params = self.reqparse.parse_args()
        api = Vehicle()
        return api.query(params = params), 200

class VehicleNewsAPI(Resource):
    """
        User API v1.0 for the Vehicle News consumer
    """
    vehicle_schema = VehicleNewsSchema()

    def __init__(self):
        ''' Setup the API '''
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('type', type=str, default='articles', location='args')
        self.reqparse.add_argument('category', type=str, location='args')
        self.reqparse.add_argument('page', type=int, default=1, location='args')
        self.reqparse.add_argument('articles_per_page', type=str, default=20, location='args')
        super(VehicleNewsAPI, self).__init__()

    @token_auth.login_required
    def get(self):
        ''' 
            Query Vehicle News consumer. 
            ------------------------
            HTTP Request method: GET
            Uses token_auth only. Needs Authorization HTTP Header with JWT Bearer token and an empty payload.
            Returns a XML when Header 'Accept: text/xml' exists.
            +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        '''
        params = self.reqparse.parse_args()
        api = VehicleNews()
        return api.query(params = params), 200


vehicle_api.add_resource(VehicleAPI, '/api/v1.0/vehicle', endpoint='vehicle.stock')
vehicle_api.add_resource(VehicleNewsAPI, '/api/v1.0/vehiclenews', endpoint='vehicle.news')

''' Requests MUST include header "Accept: application/xml" otherwise the response will break '''
@vehicle_api.representation('application/xml')
def output_xml(data, code, headers=None):
    resp = make_response(data, code)
    resp.headers.extend(headers or {})
    return resp

