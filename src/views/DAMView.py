import os, json
from flask import abort, make_response, g
from flask_restful import Resource, reqparse, fields, marshal
from ..shared.Authentication import token_auth, secret_auth, multi_auth
from ..shared.Conversion import dict2xml, dict2json
from ..models.DAM import DAM, DAMSchema
from . import dam_api

class DAMAPI(Resource):
    """
        User API v1.0 for the DAM consumer
    """
    dam_schema = DAMSchema()

    def __init__(self):
        ''' Setup the API '''
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('siteid', type=str, location='args')
        self.reqparse.add_argument('order', type=str, default='asc', location='args')
        self.reqparse.add_argument('offset', type=int, default=0, location='args')
        self.reqparse.add_argument('size', type=int, default=100, location='args')
        self.reqparse.add_argument('daysago', type=int, default=1, location='args')
        self.reqparse.add_argument('debug', type=str, location='args')
        super(DAMAPI, self).__init__()

    @token_auth.login_required
    def get(self):
        ''' 
            Query DAM consumer. 
            ------------------------
            HTTP Request method: GET
            Uses token_auth only. Needs Authorization HTTP Header with JWT Bearer token and an empty payload.
            Returns a XML when Header 'Accept: text/xml' exists.
            +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        '''
        params = self.reqparse.parse_args()
        api = DAM()
        return api.query(params = params), 200

dam_api.add_resource(DAMAPI, '/api/v1.0/dam', endpoint='dam.assets')

''' Requests MUST include header "Accept: application/xml" otherwise the response will break '''
@dam_api.representation('application/xml')
def output_xml(data, code, headers=None):
    resp = make_response(dict2xml(data), code)
    resp.headers.extend(headers or {})
    return resp

''' Requests MUST include header "Accept: application/json" otherwise the response will break '''
@dam_api.representation('application/json')
def output_json(data, code, headers=None):
    resp = make_response(dict2json(data), code)
    resp.headers.extend(headers or {})
    return resp

