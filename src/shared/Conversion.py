from xml.dom.minidom import parseString
import dicttoxml
import json, pprint

error = dict(
            status="error",
            message=None
        )

def dict2xml(data):
    global error
    if isinstance(data, (str, bytes)):
        output = dicttoxml.dicttoxml(json.loads(data.decode('utf-8')), attr_type=False)
    elif isinstance(data, dict):
        output = dicttoxml.dicttoxml(data, attr_type=False)
    else:
        error['message'] = data
        output = dicttoxml.dicttoxml(error, attr_type=False)
    dom = parseString(output)
    return dom.toprettyxml()

def dict2json(data):
    global error
    if isinstance(data, (str, bytes)):
        output = json.loads(data.decode('utf-8'))
    elif isinstance(data, dict):
        output = data
    else:
        error['message'] = data
        output = error
    return json.dumps(output, indent=2)
