from flask import g
from ..models.Security import Security
from . import token_auth, secret_auth, multi_auth

"""
    Providing multiple methods of authentication
    References
    ----------
    https://github.com/miguelgrinberg/Flask-HTTPAuth/blob/master/examples/multi_auth.py
"""

@token_auth.verify_token
def verify_token(token):
    '''
        Authenticate with JWT Token
    '''
    print('Token: {}'.format(token))
    return Security.verify_auth_token(token)

@secret_auth.verify_token
def verify_secret_key(token):
    '''
        Authenticate with Admin token in application secrets
    '''
    g.sec = Security()
    return Security.verify_secret_key(token)

