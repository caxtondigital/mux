from flask import g 
from itsdangerous import (TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired)
from marshmallow import fields, Schema
from datetime import datetime
from base64 import b64encode
from . import config, Consumer
import uuid, requests, hashlib


class VehicleNews(Consumer):
    """
        Abstract API connector.
        Making use of consumption/aggregation techniques to consume configs.
        Also making use of encapsulation to abstract away the configuration tiers.
    """

    def __init__(self):
        '''
            Initialize the Vehicle News Consumer and its consumed configurator class.
        ''' 
        self.api_url = config.get('CARMAG_API_URL')
        self.api_key = config.get('CARMAG_API_KEY')


    @property
    def headers(self):
        ''' 
            Returns the headers needed for the HTTP transaction.
        '''

        _headers = {}

        return _headers

    @property
    def parameters(self):
        ''' 
            Returns the parameters that were passed in.
        '''
        _required_parameters = dict(
                            api_key = self.api_key,
                            type = self.params.get('type', 'articles'),
                            page = self.params.get('page', 1),
                            articles_per_page = self.params.get('articles_per_page', 20)
                        )
        _optional_paramaters = dict(
                            category = self.params.get('category', None)
                        )

        _optional_paramaters = {k:v for k,v in _optional_paramaters.items() if v is not None}
        return {**_required_parameters, **_optional_paramaters}


    @property
    def url(self):
        '''
            Returns formatted URL for API query
        '''
        return self.api_url

    def query(self, params = None):
        '''
            Perform the API call to remote API endpoints
        '''
        self.params = params
        print('CARMAG_API_URL: {}'.format(config.get('CARMAG_API_URL')))
        print('JWT_SECRET: {}'.format(config.get('JWT_SECRET')))
        print('PARAMS: {}'.format(self.parameters))
        try:
            res = requests.post(self.url, params=self.parameters, headers=self.headers)
        except ConnectionError as e:
            return 'Connection error: {}'.format(e)
        except TimeoutError as e:
            return 'connection timed out: {}'.format(e)
        if res.ok:
            return res.content
        else:
            return 'Transaction error: {}, {}'.format(res.status_code, res.text)


    def __repr__(self):
        return '<id {}>'.format(self.id)

class VehicleNewsSchema(Schema):
    """
        Vehicle Schema
    """
    api_key = fields.Str(required=True)
    type = fields.Str(dump_only=True)
    category = fields.Str(dump_only=True)
    page = fields.Int(dump_only=True)
    articles_per_page = fields.Int(dump_only=True)
