from flask import g 
from itsdangerous import (TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired)
from marshmallow import fields, Schema
from datetime import datetime
from base64 import b64encode
from . import config, Consumer
import uuid, requests, hashlib


class Property(Consumer):
    """
        Abstract API connector.
        Making use of consumption/aggregation techniques to consume configs.
        Also making use of encapsulation to abstract away the configuration tiers.
    """

    def __init__(self):
        '''
            Initialize the Property Consumer and its consumed configurator class.
        ''' 
        self.api_url = config.get('PP_API_URL')
        self.api_key = config.get('PP_API_KEY')
        self.api_password = config.get('PP_API_PASSWORD')
        self.api_salt = str(uuid.uuid4().int>>64) # random 128bit UUID generated and shifted top 64 bits into bottom 64 bits position.


    @property
    def headers(self):
        ''' 
            Returns the headers needed for the HTTP transaction.
        '''
        _timestamp = datetime.now().strftime('%Y-%m-%d-%H-%M')
        _key_string = '*'.join([_timestamp,self.api_password,self.api_salt])
        _hash = hashlib.sha1(bytes(_key_string.encode('utf-8'))).digest()

        _headers = {
                        "pp-api-key": self.api_key,
                        "pp-timestamp": _timestamp, 
                        "pp-salt": self.api_salt,
                        "pp-digest": b64encode(_hash)
                    }

        return _headers

    @property
    def parameters(self):
        ''' 
            Returns the parameters that were passed in.
        '''
        _required_parameters = dict(
                            customArea = self.params.get('customArea', None),
                            sortBy = self.params.get('sortBy', 'Default'),
                            page = self.params.get('page', 1),
                            take = self.params.get('take', 30)
                        )
        _optional_paramaters = dict(
                            priceFrom = self.params.get('priceFrom', None),
                            priceTo = self.params.get('priceTo', None),
                            suburbIds = self.params.get('suburbIds'),
                            beds = self.params.get('beds', None),
                            baths = self.params.get('baths', None),
                            garages = self.params.get('garages', None),
                            parkings = self.params.get('parkings', None),
                            propertyTypeGroup = self.params.get('propertyTypeGroup', None) 
                        )

        return {**_required_parameters, **_optional_paramaters}

    @property
    def action(self):
        '''
            Return a dictionary of actions.
        '''
        _actions = dict(
                    sale = 'GetResidentialSaleResultsInCustomArea',
                    rental = 'GetResidentialRentalResultsInCustomArea'
                )
        return _actions[self.type]

    @property
    def url(self):
        '''
            Returns formatted URL for API query
        '''
        return self.api_url.format(actionName=self.action)

    def query(self, transactionType = None, params = None):
        '''
            Perform the API call to remote API endpoints
        '''
        self.type = transactionType
        self.params = params
        try:
            res = requests.get(self.url, params=self.parameters, headers=self.headers)
        except ConnectionError as e:
            return 'Connection error: {}'.format(e)
        except TimeoutError as e:
            return 'connection timed out: {}'.format(e)
        if res.ok:
            return res.content
        else:
            return 'Transaction error: {}, {}'.format(res.status_code, res.text)


    def __repr__(self):
        return '<id {}>'.format(self.id)

class PropertySchema(Schema):
    """
        Property Schema
    """
    customeArea = fields.Str(required=True)
    sortBy = fields.Str(required=True)
    page = fields.Int(required=True)
    take = fields.Int(required=True)
    priceFrom = fields.Int(dump_only=True)
    priceTo = fields.Int(dump_only=True)
    suburbIds = fields.Str(dump_only=True)
    beds = fields.Int(dump_only=True)
    baths = fields.Int(dump_only=True)
    garages = fields.Int(dump_only=True)
    parkings = fields.Int(dump_only=True)
    propertyTypeGroup = fields.Str(dump_only=True)

