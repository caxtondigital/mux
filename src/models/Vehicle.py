from flask import g 
from marshmallow import fields, Schema
from . import config, Consumer
import requests

#from itsdangerous import (TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired)
#from datetime import datetime
#from base64 import b64encode
#import uuid, requests, hashlib


class Vehicle(Consumer):
    """
        Abstract API connector.
        Making use of consumption/aggregation techniques to consume configs.
        Also making use of encapsulation to abstract away the configuration tiers.
    """

    def __init__(self):
        '''
            Initialize the Vehicle Consumer and its consumed configurator class.
        ''' 
        self.api_url = config.get('CARMAG_API_URL')
        self.api_key = config.get('CARMAG_API_KEY')


    @property
    def headers(self):
        ''' 
            Returns the headers needed for the HTTP transaction.
        '''

        _headers = {}

        return _headers

    @property
    def parameters(self):
        ''' 
            Returns the parameters that were passed in.
        '''
        _required_parameters = dict(
                            api_key = self.api_key,
                            order_by = self.params.get('order_by', 3), # Possible values: 1,2,3,4,5
                            page = self.params.get('page', 1),
                            cars_on_page = self.params.get('cars_on_page', 30)
                        )
        _optional_paramaters = dict(
                            make = self.params.get('make', None),
                            model_name = self.params.get('model_name', None),
                            bodytype = self.params.get('bodytype', None),
                            start_year = self.params.get('start_year', None),
                            end_year = self.params.get('end_year', None),
                            start_price = self.params.get('start_price', None),
                            end_price = self.params.get('end_price', None),
                            start_mileage = self.params.get('start_mileage', None),
                            end_mileage = self.params.get('end_mileage', None), 
                            type = self.params.get('type', 'all_cars'), # Possible values: all_cars, new_cars, used_cars
                            transmission = self.params.get('transmission', None),
                            fuel = self.params.get('fuel', None),  
                            location_names = self.params.get('location_names', []), # Single location names or a list of location names like - Stellenbosch, George
                            locations = self.params.get('locations', []), # Single "lat:lng:rad" descriptor: "-33.45,45.90,40" or a list of the same: "-33.45:45.90:40,-20.65:50:50:20"
                            featured = self.params.get('featured', 'Both'), # Possible values: Both, Premium, Featured
                            method = self.params.get('method', 'catchment') # Possible values: None will revert to GPS, catchment matches on Town name
                        )

        _optional_paramaters = {k:v for k,v in _optional_paramaters.items() if v is not None}
        return {**_required_parameters, **_optional_paramaters}


    @property
    def url(self):
        '''
            Returns formatted URL for API query
        '''
        return self.api_url

    def query(self, params = None):
        '''
            Perform the API call to remote API endpoints
        '''
        self.params = params
        print('CARMAG_API_URL: {}'.format(config.get('CARMAG_API_URL')))
        print('JWT_SECRET: {}'.format(config.get('JWT_SECRET')))
        print('PARAMS: {}'.format(self.parameters))
        try:
            res = requests.post(self.url, params=self.parameters, headers=self.headers)
        except ConnectionError as e:
            return 'Connection error: {}'.format(e)
        except TimeoutError as e:
            return 'connection timed out: {}'.format(e)
        if res.ok:
            return res.content
        else:
            return 'Transaction error: {}, {}'.format(res.status_code, res.text)


    def __repr__(self):
        return '<id {}>'.format(self.id)

class VehicleSchema(Schema):
    """
        Vehicle Schema
    """
    api_key = fields.Str(required=True)
    order_by = fields.Int(required=True)
    page = fields.Int(required=True)
    cars_on_page = fields.Int(required=True)
    make = fields.Str(dump_only=True)
    model_name = fields.Str(dump_only=True)
    bodytype = fields.Str(dump_only=True)
    start_year = fields.Int(dump_only=True)
    end_year = fields.Int(dump_only=True)
    start_price = fields.Int(dump_only=True)
    end_price = fields.Int(dump_only=True)
    start_mileage = fields.Int(dump_only=True)
    end_mileage = fields.Int(dump_only=True)
    type = fields.Str(dump_only=True)
    transmission = fields.Str(dump_only=True)
    fuel = fields.Int(dump_only=True)
    location_names = fields.Str(dump_only=True)
    locations = fields.Str(dump_only=True)
    featured = fields.Str(dump_only=True)
    method = fields.Str(dump_only=True)

