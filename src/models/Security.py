from itsdangerous import TimedJSONWebSignatureSerializer, JSONWebSignatureSerializer 
from itsdangerous import BadHeader, BadSignature, SignatureExpired
from . import config

class Serializer(TimedJSONWebSignatureSerializer):

    def loads(self, s, salt=None, return_header=False):
        payload, header = JSONWebSignatureSerializer.loads(
            self, s, salt, return_header=True
        )

        segment = header if 'exp' in header else payload if 'exp' in payload else None

        print('segment = {}'.format(segment))

        if segment is None:
            raise BadSignature("Missing expiry date", payload=payload)

        int_date_error = BadHeader("Expiry date is not an IntDate", payload=payload)
        try:
            segment["exp"] = int(segment["exp"])
        except ValueError:
            raise int_date_error
        if segment["exp"] < 0:
            raise int_date_error

        if segment["exp"] < self.now():
            raise SignatureExpired(
                "Signature expired",
                payload=payload,
                date_signed=self.get_issue_date(segment),
            )

        if return_header:
            return payload, header
        return payload


class Security(object):
    """
        Security class.
    """

    def generate_auth_token(self, expiration=600):
        s = Serializer(config.get('JWT_SECRET','Setec Astronomy'), expires_in=expiration)
        return s.dumps({'key': config.get('APP_SECRET')})

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(config.get('JWT_SECRET'),'Setec Astronomy')
        try:
            data, header = s.loads(token, return_header=True) 
        except SignatureExpired as e:
            print('Signature expired: {}'.format(e))
            return None    # valid token, but expired
        except BadSignature as e:
            print('Bad signature: {}'.format(e))
            print('JWT_SECRET: {}'.format(config.get('JWT_SECRET')))
            print('token: {}'.format(token))
            return None    # invalid token
        return True

    @staticmethod
    def verify_secret_key(token):
        return token == config.get('APP_SECRET') 
