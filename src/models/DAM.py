from flask import g
from itsdangerous import (TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired)
from marshmallow import fields, Schema
from datetime import datetime
from base64 import b64encode
from pynq import From
from . import config, Consumer, MatchAssetsUsingPYNQ as MatchAssets
import uuid, requests, hashlib, json, re
from pprint import pprint


class DAM(Consumer):
    """
        DAM API Consumer.
    """

    def __init__(self):
        '''
            Initialize the DAM Consumer and its consumed configurator class.
        ''' 
        self.api_url = config.get('ELASTICSEARCH_API_URL')
        self.api_action = '_search'
        self.unit = 'local_newspapers'
        self.delta = 'now/d-1d'
        self.site_article_map = None


    @property
    def headers(self):
        ''' 
            Returns the headers needed for the HTTP transaction.
        '''
        _headers = {
                        "Accept": "text/json",
                        "Content-Type": "application/json"
                   }

        return _headers

    @property
    def parameters(self):
        ''' 
            Returns the parameters that were passed in.
        '''
        _required_parameters = dict(
                            siteid = self.params.get('siteid', None),
                            offset = self.params.get('offset', 0),
                            size = self.params.get('size', 100),
                            order = self.params.get('order', 'asc')
                        )
        _optional_paramaters = dict(
                            delta = 'now/d-{}d'.format(self.params.get('daysago', 1))
                        )

        return {**_required_parameters, **_optional_paramaters}

    @property
    def articles_payload(self):
        '''
            Returns the JSON payload body for ElasticSearch queries.
            This one queries the DAM ElasticSearch index for posts only.
        '''
      
        filters = [
            { "match": { "business": self.business }},
            { "match": { "asset_type": "post" }},
            { "match": { "revision": "No" }},
            { "range": { "created_date_time": { "gte": self.parameters.get('delta', self.delta) }}}
        ]

        if (self.parameters.get('siteid', None)):
            filters.append({ "match": { "site_id": self.parameters.get('siteid') }})

        #print("PARAMETERS: {}".format(self.parameters))
        #print("FILTERS: {}".format(filters))

        # Nothing yet - maybe later
        must_not = []
 
        _payload = {
            "query": {
                "bool": {
                    "must": {
                        "match_all": {}
                    },
                    "filter": filters,
                    "must_not": must_not,
                }
            },
            "sort": { "created_date_time": { "order": self.parameters.get('order', 'asc')} },
            "size": self.parameters.get('size'),
            "from": self.parameters.get('offset')
        }

        return _payload

    @property
    def attachments_payload(self):
        '''
            Returns the JSON payload body for ElasticSearch queries.
            This one queries the DAM ElasticSearch index for attachments that
            Have the supplied site_id and article_id.
        '''
      
        filters = [
            { "match": { "business": self.business }},
            { "match": { "asset_type": "attachment" }},
            { "match": { "revision": "No" }}
        ]

        #print("PARAMETERS: {}".format(self.parameters))
        #print("FILTERS: {}".format(filters))

        _payload = {
            "query": {
                "bool": {
                    "must": [], 
                    "filter": filters
                }
            },
            "size": "10000",
            "from": "0"
        }
        #pprint(_payload)
        return _payload

    @property
    def payload(self):
        pass

    @property
    def action(self):
        '''
            Returns the action for the DAM endpoint
        '''
        return self.api_action

    @property
    def business(self):
        '''
            Returns the business unit
        '''
        return self.unit
    
    @property
    def url(self):
        '''
            Returns formatted URL for API query
        '''
        return self.api_url.format(action=self.api_action, business=self.business)

    def query(self, params = None):
        '''
            Perform the API call to remote API endpoints
        '''
        self.params = params     
        searcher = DAMIndex(
                    url=self.url,
                    headers=self.headers,
                    articles_payload=self.articles_payload,
                    attachments_payload=self.attachments_payload,
                    debug=self.params.get('debug', None)
                )
        
        return searcher.buildResults()

    def __repr__(self):
        return '<id {}>'.format(self.id)



class DAMSchema(Schema):
    """
        DAM Schema
    """
    siteid = fields.Integer(required=False)
    order = fields.String(required=False, default='asc')
    size = fields.Integer(required=False, defaut=100)
    offset = fields.Integer(required=False, default=0)
    delta = fields.String(required=False)
    debug = fields.String(required=False)

class Struct(object):
    """
        For creating Python Objects from dictionaries so that PYNQ can query the data.
    """
    def __init__(self, data):
        for a, b in data.items():
            if isinstance(b, (list, tuple)):
                setattr(self, a, [Struct(x) if isinstance(x, dict) else x for x in b])
            else:
                setattr(self, a, Struct(b) if isinstance(b, dict) else b)

    @staticmethod
    def make_dict(obj):
        '''
            Create a dictionary from the Python Object.
        '''
        ret = vars(obj)
        ret['_source'] = vars(ret['_source'])
        return ret

class DAMIndex(object):
    """
        For performing complex searches on the DAM ElasticSearch Index
    """
    
    url = None
    headers = None
    articles_payload = None
    attachments_payload = None
    debug = False
    last_error = None


    def __init__(self, **kwargs):
        self.url = kwargs.get('url', None)
        self.headers = kwargs.get('headers', None)
        self.articles_payload = kwargs.get('articles_payload', None)
        self.attachments_payload = kwargs.get('attachments_payload', None)
        self.debug = kwargs.get('debug', None)

    def buildResults(self):
        ''' 
            Fetch the articles based on the provided parameters
            and handle all the possible errors accordingly.
        '''
        articles_result = self.getResults(self.articles_payload)
        if not isinstance(articles_result, object):
            self.last_error = 'Unknown error occurred' if not self.last_error else self.last_error
            return False
        if not articles_result.ok:
            self.last_error = 'Transaction error: {}, {}'.format(articles_result.status_code, articles_result.text)
            return False

        articles_dict = json.loads(articles_result.content.decode('utf-8'))
        articles_obj = Struct(articles_dict)
        ''' 
            Build a collection of two lists i.e 1) article_ids and 2) site_ids for the attachments query
            using set comprehension '{}' instead of list comprehension '[]'
        '''
        site_article_map = dict(
                    site_id = {x._source.site_id for x in articles_obj.hits.hits},
                    asset_id = {x._source.attachment_id for x in articles_obj.hits.hits}
                )

        self.attachments_payload['query']['bool']['must'] = [
                    { "terms": { "site_id": [str(x) for x in site_article_map['site_id'] if x is not None]}},
                    { "terms": { "asset_id": [str(x) for x in site_article_map['asset_id'] if x is not None]}}
                ]

        ''' 
            Fetch the attachments that belong to the retrieved articles
            and handle all the possible errors accordingly.
        '''
        attachments_result = self.getResults(self.attachments_payload)
        if not isinstance(attachments_result, object):
            self.last_error = 'Unknown error occurred' if not self.last_error else self.last_error
            return False
        if not attachments_result.ok:
            self.last_error = 'Transaction error: {}, {}'.format(attachments_result.status_code, attachments_result.text)
            #print(self.last_error)
            return False

        attachments_dict = json.loads(attachments_result.content.decode('utf-8'))
        attachments_obj = Struct(attachments_dict)

        '''
            Now use the imported Matcher to find and match attachments
        '''
        if self.debug and 'articles' in self.debug:
            return articles_dict
        if self.debug and 'attachments' in self.debug:
            return attachments_dict

        matcher = MatchAssets(articles_obj, attachments_obj)
        matched_obj = matcher.match() 
        articles_dict['hits']['hits'] = list(map(Struct.make_dict, matched_obj.hits.hits))
        return articles_dict

    def getResults(self, payload):
        try:
            return requests.get(self.url, headers=self.headers, json=payload)
        except ConnectionError as e:
            self.last_error = 'Connection error: {}'.format(e)
        except TimeoutError as e:
            self.last_error = 'Connection timed out: {}'.format(e)
        return False


