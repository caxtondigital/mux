from flask import g
from itsdangerous import (TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired)
from marshmallow import fields, Schema
from datetime import datetime
from base64 import b64encode
from . import config, Consumer
import uuid, requests, hashlib


class Catalogue(Consumer):
    """
        Catalogue API Consumer.
    """

    def __init__(self):
        '''
            Initialize the Catalogue Consumer and its consumed configurator class.
        ''' 
        self.api_url = config.get('GUZZLE_API_URL')
        self.api_action = 'catalogues'


    @property
    def headers(self):
        ''' 
            Returns the headers needed for the HTTP transaction.
        '''
        _headers = dict()

        return _headers

    @property
    def parameters(self):
        ''' 
            Returns the parameters that were passed in.
        '''
        _required_parameters = {'format': self.params.get('format', None)}
        _optional_paramaters = dict(
                            gps = self.params.get('gps', 'disabled'),
                            radius = self.params.get('radius', '100.0'),
                            order_by = self.params.get('order_by', 'id'),
                            limit = self.params.get('limit', '10'),
                            offset = self.params.get('offset', '0'),
                            paying_or_trial = self.params.get('paying_or_trial', False),
                            supplier_id = self.params.get('supplier_id', None),
                            category_id = self.params.get('category_id', None),
                            category_foreign_key_id = self.params.get('foreign_key_category_id', None),
                            province = self.params.get('province', None) 
                        )

        return {**_required_parameters, **_optional_paramaters}

    @property
    def action(self):
        '''
            Returns the action for the Catalogues andpoint
        '''
        return self.api_action
    
    @property
    def url(self):
        '''
            Returns formatted URL for API query
        '''
        return self.api_url.format(actionName=self.api_action)

    def query(self, params = None):
        '''
            Perform the API call to remote API endpoints
        '''
        self.params = params
        try:
            res = requests.get(self.url, params=self.parameters, headers=self.headers)
        except ConnectionError as e:
            return 'Connection error: {}'.format(e)
        except TimeoutError as e:
            return 'connection timed out: {}'.format(e)
        if res.ok:
            return res.content
        else:
            return 'Transaction error: {}, {}'.format(res.status_code, res.text)


    def __repr__(self):
        return '<id {}>'.format(self.id)



class CatalogueSchema(Schema):
    """
        Catalogue Schema
    """
    format = fields.String(required=True) # format is a reserved word so this might cause issues.
    gps = fields.String(required=False, default='disabled')
    radius = fields.Float(required=False, default=100.0)
    order_by = fields.String(required=False, default='id')
    limit = fields.Integer(required=False, defaut=10)
    offset = fields.Integer(required=False, default=0)
    paying_or_trial = fields.Boolean(required=False)
    supplier_id = fields.Integer(required=False)
    category_id = fields.Integer(required=False)
    category_foreign_key_id = fields.Integer(required=False)
    province = fields.String(required=False)

