from abc import ABC, abstractmethod
from pynq import From
import re

config = {}

class Consumer(ABC):
    """ 
        Abstract class that contains the basic functionality for API consumers.
        Either inherit from imported ABC or set __metaclass__ equal to imported ABCMeta.
    """

    @abstractmethod
    def __init__(self, *args, **kwargs):
        '''
            Initialize the object with config
        '''
        pass

    @property
    @abstractmethod
    def headers(self, *args, **kwargs):
        '''
            Setup the security headers
        '''
        pass

    @property
    @abstractmethod
    def parameters(self, *args, **kwargs):
        '''
            Setup the query parameters
        '''
        pass

    @property
    @abstractmethod
    def url(self, *args, **kwargs):
        '''
            Setup the api url
        '''
        pass

    @abstractmethod
    def query(self, *args, **kwargs):
        '''
            Perform the HTTP request
        '''
        pass


class MatchAssetsBase(object):

    regex = "(.*)\ src=\\\"_func\/readImages\.php\?file=local_newspapers(\S*)\\\"(.*)"
    subst = "\\1 src=\"{site_url}/wp-content/uploads\\2\"\\3"
    
    def __init__(self, articles_obj, attachments_obj):
        self.articles_obj = articles_obj
        self.attachments_obj = attachments_obj


class MatchAssetsUsingPYNQ(MatchAssetsBase):
    """
        ############################################################################################
        Using PYNQ
        --------------------------------------------------------------------------------------------
        Running through two dictionaries matching attachments to articles
        using the Python-Pynq which is an implementation of a subset of
        the LINQ package available for .NET.

        Extract the articles featured image link from the attachments_obj Python Object and insert it
        into the articles_link in the articles_obj

        ############################################################################################
    """

    def match(self):

        if not self.articles_obj or not self.attachments_obj:
            return False

        for x in self.articles_obj.hits.hits:
            params = dict(
                        site_url = x._source.site_url,
                        site_id = x._source.site_id,
                        asset_id = x._source.attachment_id
                    )

            result = re.sub(self.regex, self.subst.format(**params), x._source.content, 0, re.MULTILINE)
            if result: x._source.content = result

            query = "item._source.site_id == '{site_id}' \
                    and item._source.asset_id == '{asset_id}'".format(**params)

            attachment = From(self.attachments_obj.hits.hits).where(query).select_many()

            if attachment:
                params.update(
                            attachment_link = attachment[0]._source.attachment_link
                        )
                x._source.attachment_link = '{site_url}/wp-content/uploads/sites/{site_id}/{attachment_link}'.format(**params)

        return self.articles_obj



#class MatchAssetsUsingPythonQL(MatchAssetsBase):
#    """
#        ############################################################################################
#        Alternative way using PythonQL.
#        --------------------------------------------------------------------------------------------
#        Running through two dictionaries matching attachments to articles
#        using the awesome PythonQL preprocessor which extends the default
#        Python comprehensions functionality.
#
#        1. Need to add PythonQL3 to the requirements/production.txt
#        2. Need to add the line `#coding: pythonql` at the top of this file
#
#        ############################################################################################
#    """
#
#    def match(self):
#
#        if not self.articles_obj or not self.attachments_obj:
#            return False
#
#        self.articles_obj.hits.hits = [
#                    select x
#                    for x in self.articles_obj.hits.hits,
#                        y in self.attachments_obj.hits.hits
#                    group by x
#                    let x._source.attachment_link = '{site_url}/wp-content/uploads/sites/{site_id}/{attachment_link}'.format(dict(
#                            site_url = x._source.site_url,
#                            site_id = x._source.site_id,
#                            attachment_link = y._source.attachment_link
#                        ))
#                    where y._source.article_id = x._source.article_id
#                    and y._source.site_id = x._source.site_id
#                    let x._source.content = try re.sub(self.regex, self.subst.format(dict(
#                            site_url = x._source.site_url)), x._source.content, 0, re.MULTILINE) except x._source.content
#                ]
#
#        return self.articles_obj

