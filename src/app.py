import os
from flask import Flask, Blueprint
from flask_restful import Api
from .models import config
from .views.PropertyView import property_api, PropertyAPI
from .views.CatalogueView import catalogue_api, CatalogueAPI
from .views.VehicleView import vehicle_api, VehicleAPI
from .views.DAMView import dam_api, DAMAPI
from pprint import pprint

def create_app():
    app = Flask(__name__)
    app.config.from_mapping(os.environ)
    config.update(app.config)
    
    # Property Blueprint
    property_blueprint = Blueprint('property', __name__)
    property_api.init_app(property_blueprint)
    app.register_blueprint(property_blueprint)

    # Catalogue Blueprint
    catalogue_blueprint = Blueprint('catalogue', __name__)
    catalogue_api.init_app(catalogue_blueprint)
    app.register_blueprint(catalogue_blueprint)

    # Vehicle Blueprint
    vehicle_blueprint = Blueprint('vehicle', __name__)
    vehicle_api.init_app(vehicle_blueprint)
    app.register_blueprint(vehicle_blueprint)

    # DAM Blueprint
    dam_blueprint = Blueprint('dam', __name__)
    dam_api.init_app(dam_blueprint)
    app.register_blueprint(dam_blueprint)

    @app.route('/')
    def index():
        return "<h1 style='color:red'> MUX </h1>"

    return app
