#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset
set -o xtrace

# Remember to use gunicorn scripts to start the app
#python manage.py runserver --host="0.0.0.0" --port="8000"
/usr/bin/supervisord && supervisorctl start mux  
