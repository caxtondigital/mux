#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset
set -o xtrace

python manage.py runserver --host="0.0.0.0" --port="8000"
